
using System.Data;



namespace calculator
{
    public partial class pantalla : Form
    {
        public pantalla()
        {
            InitializeComponent();
        }
        double nun1, nun2 = 0;
        string op, x;   
        private void btn1_Click(object sender, EventArgs e)
        {

            if (txtresultado.Text == "")
            {
               
                txtresultado.Text = "1";    
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "1";
            }

        }
        private void btn2_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "2";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "2";
            }
        }
        private void btn3_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "3";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "3";
            }
        }
        private void btn4_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "4";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "4";
            }
        }
        private void btn5_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "5";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "5";
            }
        }
        private void btn6_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "6";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "6";
            }
        }
        private void btn7_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "7";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "7";
            }
        }
        private void btn8_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "8";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "8";
            }
        }
        private void btn9_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "9";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "9";
            }
        }
        private void btn0_Click(object sender, EventArgs e)
        {
            if (txtresultado.Text == "")
            {

                txtresultado.Text = "0";
            }
            else
            {
                txtresultado.Text = txtresultado.Text + "0";
            }
        }
        private void btnlimpiar_Click(object sender, EventArgs e)
        {
            txtresultado.Clear();
        }
        private void btnigual_Click(object sender, EventArgs e)
        {
            nun2 = Convert.ToDouble(this.txtresultado.Text);   
            switch (x)
            {
                case "+":
                    this.txtresultado.Text = Convert.ToString(nun2 + nun1);
                    insertar_Suma();
                    break;
                case "-":
                    this.txtresultado.Text = Convert.ToString(nun2 - nun1);
                    insertar_resta();
                    break;
                case "*":
                    this.txtresultado.Text = Convert.ToString(nun2 * nun1);
                    insertar_multiplicacion();
                    break;
                case "/":
                    this.txtresultado.Text = Convert.ToString(nun2 / nun1);
                    insertar_divicion();
                    break;
            }
            
        }
        private void btnrestar_Click(object sender, EventArgs e)
        {
            nun1 = double.Parse(txtresultado.Text);
            x = "-";
            this.txtresultado.Clear();
            this.txtresultado.Focus();
        }
        private void btnmultiplicar_Click(object sender, EventArgs e)
        {
            nun1 = double.Parse(txtresultado.Text);
            x = "*";
            this.txtresultado.Clear();
            this.txtresultado.Focus();
        }
        private void btndividir_Click(object sender, EventArgs e)
        {
            nun1 = double.Parse(txtresultado.Text);
            x = "/";
            this.txtresultado.Clear();
            this.txtresultado.Focus();
        }

        private void btnhistorial_Click(object sender, EventArgs e)
        {
            dgvhistorial.Visible = true;
            dgvhistorial.Dock = DockStyle.Fill;
            mostrarsuma();
        }

        private void btnpunto_Click(object sender, EventArgs e)
        {
            if (this.txtresultado.Text.Contains(".") == false)
            {
                txtresultado.Text = txtresultado.Text + ".";

            }
        }
        private void btnsumar_Click(object sender, EventArgs e)
        {
            nun1 = double.Parse(txtresultado.Text);
            x = "+";
            this.txtresultado.Clear();
            this.txtresultado.Focus(); 
        }   
       private void insertar_Suma()
        {

            Dsuma funcion = new Dsuma();
            lsuma parametros = new lsuma();
            parametros.numero1 = Convert.ToString(nun1);
            parametros.simbolo = x;
            parametros.numero2 = Convert.ToString(nun2);
            parametros.numero3 = txtresultado.Text;
            funcion.inserta_Dsuma(parametros);
            
        }
        private void insertar_resta()
        {
            Dresta funcion = new Dresta(); 
            lresta parametros = new lresta();
            parametros.numero1 = Convert.ToString(nun1);
            parametros.simbolo = x;
            parametros.numero2 = Convert.ToString(nun2);
            parametros.numero3 = txtresultado.Text;
            funcion.inserta_Dresta(parametros);
        }
        private void insertar_divicion()
        {
            ddivision funcion = new ddivision();
            Ldivision parametros = new Ldivision();
            parametros.numero1 = Convert.ToString(nun1);
            parametros.simbolo = x;
            parametros.numero2 = Convert.ToString(nun2);
            parametros.numero3 = txtresultado.Text;
            funcion.inserta_Dividir(parametros);
        }
        private void insertar_multiplicacion()
        {
            Dmultiplicacion funcion = new Dmultiplicacion();
            Lmultiplacion parametros= new Lmultiplacion();
            parametros.numero1 = Convert.ToString(nun1);
            parametros.simbolo = x;
            parametros.numero2 = Convert.ToString(nun2);
            parametros.numero3 = txtresultado.Text;
            funcion.inserta_Multiplicacion(parametros);
        }
        private void mostrarsuma()
        {
            lsuma parametros = new lsuma();
            DataTable dt = new DataTable(); 
            Dsuma funcion = new Dsuma();
            funcion.mostrar_suma(ref dt);
            dgvhistorial.DataSource = dt;
        }
    }
}  
