﻿namespace calculator
{
    partial class historial2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvhistorial = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvhistorial)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvhistorial
            // 
            this.dgvhistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvhistorial.Location = new System.Drawing.Point(12, 12);
            this.dgvhistorial.Name = "dgvhistorial";
            this.dgvhistorial.RowTemplate.Height = 25;
            this.dgvhistorial.Size = new System.Drawing.Size(311, 208);
            this.dgvhistorial.TabIndex = 0;
            // 
            // historial2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 283);
            this.Controls.Add(this.dgvhistorial);
            this.Name = "historial2";
            this.Text = "historial2";
            ((System.ComponentModel.ISupportInitialize)(this.dgvhistorial)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgvhistorial;
    }
}