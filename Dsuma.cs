﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace calculator
{
    public class Dsuma
    {
        
       public bool inserta_Dsuma(lsuma parametros)
        {
            try
            {
                Conexionmaestra.abrir();
                SqlCommand cmd =  new SqlCommand("suma", Conexionmaestra.conectar);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@a",parametros.numero1);
                cmd.Parameters.AddWithValue("@s", parametros.simbolo);
                cmd.Parameters.AddWithValue("@b", parametros.numero2);
                cmd.Parameters.AddWithValue("@c", parametros.numero3);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
                return false;
            }
            finally
            {
                Conexionmaestra.cerrar();
            }
        }
        public void mostrar_suma(ref DataTable dt)
        {
            try
            {
                Conexionmaestra.abrir();
                lsuma parametros = new lsuma();
                SqlDataAdapter da = new SqlDataAdapter("mostrar_suma", Conexionmaestra.conectar);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.SelectCommand.Parameters.AddWithValue("@oper", parametros.numero1);
                da.SelectCommand.Parameters.AddWithValue("@sim", parametros.simbolo);
                da.SelectCommand.Parameters.AddWithValue("@operan", parametros.numero2);
                da.SelectCommand.Parameters.AddWithValue("@resul", parametros.numero3);
                da.Fill(dt);

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            finally
            {
                Conexionmaestra.cerrar();
            }

        }
        
      
    }
}
